importScripts('https://www.gstatic.com/firebasejs/8.0.2/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/8.0.2/firebase-messaging.js');

var firebaseConfig = {
    apiKey: "AIzaSyCEYZ73K6WelOFy9dIZ1HhRiyZVYZv1ZjM",
    authDomain: "smsvuihocflatform.firebaseapp.com",
    databaseURL: "https://smsvuihocflatform.firebaseio.com",
    projectId: "smsvuihocflatform",
    storageBucket: "smsvuihocflatform.appspot.com",
    messagingSenderId: "256385434516",
    appId: "1:256385434516:web:ac13f1a86a1644f0010581",
    measurementId: "G-RBZYHLTM2G"
};

firebase.initializeApp(firebaseConfig);
const messaging = firebase.messaging();

messaging.setBackgroundMessageHandler(function (payload) {
    console.log(payload);
    const notification=JSON.parse(payload);
    const notificationOption = {
        body:notification.body,
        icon:notification.icon
    };
    return self.registration.showNotification(payload.notification.title,notificationOption);
});